"""djangoapp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.contrib import admin
from django.urls import path, reverse_lazy
from django.contrib.auth.decorators import login_required
from . import views

urlpatterns = [
    path(
        "",
        login_required(
            views.CreateReservationView.as_view(), login_url=reverse_lazy("login")
        ),
        name="home",
    ),
    path("day", login_required(views.DayView.as_view()), name="day"),
    path(
        "delete",
        login_required(views.DeleteReservation.as_view()),
        name="delete_reservation",
    ),
    path("login", views.LoginView.as_view(), name="login"),
    path("logout", views.LogoutView.as_view(), name="logout"),
    path("create/<token>", views.CreateUserView.as_view(), name="create_user"),
    path("admin/", admin.site.urls),
    path(
        "calendar/<year>/<month>",
        login_required(views.CalendarView.as_view()),
        name="calendar",
    ),
    path("message", login_required(views.MessageView.as_view()), name="message"),
    path("profile", login_required(views.ChangeUserDataView.as_view()), name="profile"),
    path(
        "change_password",
        login_required(views.ChangePasswordView.as_view()),
        name="change_password",
    ),
    path(
        "my_reservations",
        login_required(views.MyReservationsView.as_view()),
        name="my_reservations",
    ),
    path(
        "reservation/<hid>",
        login_required(views.EditReservationView.as_view()),
        name="reservation",
    ),
]
