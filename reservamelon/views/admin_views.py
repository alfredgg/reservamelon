from django.views.generic import UpdateView, View, CreateView, TemplateView
from reservamelon.forms.pydantic_form import PydanticForm
from django.contrib import admin
from django.contrib.sites.models import Site
from django.http import HttpResponseRedirect, HttpResponse
from reservamelon.models.site_extension import SettingsSchema
from reservamelon.models import Reservation, Key
from django.urls import reverse
from django.contrib import messages
from django.core.exceptions import PermissionDenied
from django.utils import timezone
import csv
from reservamelon.schemas import ExportSchema, ExportInstanceSchema
from random import randint
from reservamelon.hashid import encode_hashid


def is_superuser(view_func):
    def _wrapped_view(request, *args, **kwargs):
        if not request.user.is_superuser:
            raise PermissionDenied
        return view_func(request, *args, **kwargs)

    return _wrapped_view


class SettingsView(UpdateView):
    form_class = PydanticForm
    schema = SettingsSchema
    template_name = "admin/settings.html"

    def get_object(self, queryset=None):
        values = Site.objects.get(pk=self.kwargs["id"]).ext
        return values

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.pop("instance", None)  # This is not a ModelForm
        kwargs["initial"] = self.object.settings.dict()
        kwargs["schema"] = self.schema
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(admin.site.each_context(self.request))
        context["title"] = "Settings"
        context["subtitle"] = Site.objects.get(pk=self.kwargs["id"]).name
        return context

    def form_valid(self, form):
        self.object.settings = form.cleaned_data
        self.object.save()
        messages.add_message(
            self.request, messages.SUCCESS, "Settings updated successfully"
        )
        return HttpResponseRedirect(
            redirect_to=reverse("admin:sites_site_change", args=self.kwargs["id"])
        )


class ExportView(View):
    def get(self, request, *args, **kwargs):
        reservations = Reservation.objects.prefetch_related("element", "name").all()
        export = ExportSchema.parse_obj(list(reservations))
        export_time = timezone.now().strftime("%y%m%d%H%M")
        response = HttpResponse(
            content_type="text/csv",
            headers={
                "Content-Disposition": f'attachment; filename="reservations.{export_time}.csv"'
            },
        )
        writer = csv.DictWriter(
            response, fieldnames=ExportInstanceSchema.__fields__.keys(), delimiter=";"
        )
        writer.writeheader()
        writer.writerows(export.dict())
        return response


class GenerateUserView(CreateView):
    model = Key
    fields = (
        "first_name",
        "last_name",
        "namespaces"
    )
    template_name = "admin/generate_user.html"

    def form_valid(self, form):
        self.key = form.save()
        self.key.token = encode_hashid(randint(1000, 11000))
        self.key.save()
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse("admin:reservamelon_key_change", args=(self.key.pk,))
