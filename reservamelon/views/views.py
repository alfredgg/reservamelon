from django.views.generic import (
    TemplateView,
    CreateView,
    RedirectView,
    FormView,
    UpdateView,
)
from reservamelon.forms import (
    ReservationForm,
    MessageForm,
    SignUpForm,
    ChangeUserDataForm,
    ChangePasswordForm,
    LoginForm,
    PasswordlessForm,
    ReservationUpdateForm,
)
from django.urls import reverse, reverse_lazy
from datetime import datetime, date, time, timedelta
from django.utils import timezone
from reservamelon.models import Element, User, ReservationBase, Key, Reservation
import json
from django.contrib import messages
from django.contrib.auth.views import (
    LoginView as DjangoLoginView,
    LogoutView as DjangoLogoutView,
)
from django.http import HttpResponseRedirect, Http404
from django.contrib.sites.shortcuts import get_current_site
from reservamelon.schemas import CalendarSchema
from django.core.exceptions import PermissionDenied
from reservamelon.hashid import decode_hashid
from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.conf import settings
from django.contrib.auth import login


class HTTPResponseHXRedirect(HttpResponseRedirect):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self["HX-Redirect"] = self["Location"]

    status_code = 200


class GetElementMomentMixin:
    def get_element_moment_dict(self):
        return dict(
            moment=datetime.combine(
                date.fromisoformat(self.request.GET["moment"]),
                time(0, 0),
                tzinfo=timezone.get_current_timezone(),
            ),
            element=self.request.GET["element"],
        )


class CreateReservationView(GetElementMomentMixin, CreateView):
    form_class = ReservationForm
    template_name = "reservamelon/reservation_form.html"

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["user"] = self.request.user
        if "element" in self.request.GET.keys() and "moment" in self.request.GET.keys():
            kwargs["initial"] = self.get_element_moment_dict()
        return kwargs

    def form_valid(self, form):
        if not form.cleaned_data["element"].is_user_allowed(self.request.user):
            return HttpResponseRedirect(self.request.META.get("HTTP_REFERER"))

        self.object = form.save(commit=False)
        self.object.name = self.request.user
        self.object.moment_end = self.object.moment + timedelta(
            minutes=int(float(form.cleaned_data["hours"]) * 60)
        )
        self.object.save()

        messages.add_message(
            self.request,
            messages.SUCCESS,
            f"S'ha reservat correctament {self.object.element} per {self.object.name} des de les {self.object.moment.time()} a les {self.object.moment_end.time()}",
        )
        return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, form):
        for errors in form.errors.values():
            for error in errors:
                messages.add_message(self.request, messages.ERROR, error)
        return HttpResponseRedirect(
            f"{reverse('home')}?element={form.data['element']}&moment={form.data['moment']}"
        )

    def get_success_url(self):
        return f"{reverse('home')}?element={self.object.element.hashid}&moment={self.object.moment.date()}"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        moment = context["form"].fields["moment"].initial
        if "moment" in self.request.GET.keys():
            moment = self.get_element_moment_dict()["moment"].date()
        context.update(
            {
                "smoment": str(moment),
                "date": moment,
                "details": [
                    {
                        "id": element.hashid,
                        "description": element.html_description,
                        "image": element.picture.image.url if element.picture else None,
                    }
                    for element in Element.objects.all()
                    if element.description or element.picture
                ],
            }
        )
        return context


class DayView(GetElementMomentMixin, TemplateView):
    template_name = "reservamelon/day.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        element_moment = self.get_element_moment_dict()
        element_hid, day = element_moment["element"], element_moment["moment"].date()
        element = Element.objects.get(pk=decode_hashid(element_hid))
        context["date"] = day
        context["moment"] = str(day)
        context["element"] = element

        if not context["element"].is_user_allowed(self.request.user):
            raise PermissionDenied
        day_min = datetime.combine(day, time.min).astimezone(
            timezone.get_current_timezone()
        )
        day_max = datetime.combine(day, time.max).astimezone(
            timezone.get_current_timezone()
        )
        reservations = ReservationBase.get_intersections(element, day_min, day_max)
        current_hour = lambda x: datetime.combine(
            day,
            time.fromisoformat(f"{x:02d}:00:00"),
            tzinfo=timezone.get_current_timezone(),
        )
        now = datetime.now(tz=timezone.get_current_timezone())
        if day == now.date():
            context["hour_now"] = now.hour
        if day < now.date():
            context["hour_now"] = 25
        elif day > now.date():
            context["hour_now"] = -1
        context["reservations"] = [
            (
                i,
                [
                    intersection
                    for intersection in reservations
                    if intersection.intersects(
                        current_hour(i), current_hour(i) + timedelta(hours=1)
                    )
                ],
            )
            for i in range(24)
        ]
        context["json_reservations"] = json.dumps(
            {str(reservation.hashid): str(reservation) for reservation in reservations}
        )
        return context


class DeleteReservation(RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        reservation = ReservationBase.objects.get(
            pk=decode_hashid(self.request.GET["reservation"])
        )
        element = reservation.element.hashid
        reservation_date = reservation.moment.date()
        reservation.delete()
        if reservation.name != self.request.user:
            return f"{reverse('home')}?element={element}&moment={reservation_date}"
        return f"{reverse('home')}?element={element}&moment={reservation_date}"


class LoginView(DjangoLoginView):
    template_name = "reservamelon/login.html"
    next_page = reverse_lazy("home")

    def get_form_class(self):
        get_current_site(self.request).ext.refresh_from_db()
        if get_current_site(self.request).ext.settings.passwordless:
            return PasswordlessForm
        return LoginForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["current_site"] = get_current_site(self.request)
        return context


class LogoutView(DjangoLogoutView):
    next_page = "login"

    def dispatch(self, request, *args, **kwargs):
        response = super().dispatch(request, *args, **kwargs)
        if type(response) == HttpResponseRedirect:
            return HTTPResponseHXRedirect(redirect_to=response.url)
        return response


class CalendarView(TemplateView):
    template_name = "reservamelon/calendar.html"

    def get_context_data(self, **kwargs):
        month = kwargs.get("month")
        year = kwargs.get("year")
        calendar = CalendarSchema.create_for(int(month), int(year))

        if pk := decode_hashid(self.request.GET.get("element")):
            element = Element.objects.get(pk=pk)
            if not element.is_user_allowed(self.request.user):
                raise PermissionDenied
            first_date = timezone.make_aware(
                datetime.combine(calendar.rows[0][0].date, datetime.min.time()),
                timezone.utc,
            )
            last_date = timezone.make_aware(
                datetime.combine(calendar.rows[-1][-1].date, datetime.min.time()),
                timezone.utc,
            )
            reservations = ReservationBase.get_intersections(
                element=element, moment_start=first_date, moment_end=last_date
            ).all()
            hours_per_day = [
                hour_day
                for reservation in reservations
                for hour_day in reservation.hours_per_day()
            ]
            dates = set(date for date, _ in hours_per_day)
            count_hours = {current: 0 for current in dates}
            for day, hours in hours_per_day:
                count_hours[day] += hours
            for row in calendar.rows:
                for day in row:
                    hours = count_hours.get(day.date, 0)
                    if not hours:
                        day.color = "white"
                        continue
                    day.color = "alert" if hours < element.threshold else "full"

        context = super().get_context_data(**kwargs)
        context["calendar"] = calendar
        return context


class CreateUserView(CreateView):
    model = User
    form_class = SignUpForm
    template_name = "reservamelon/simple_form.html"

    def get_key(self):
        try:
            return Key.objects.get(token=self.kwargs["token"])
        except Key.DoesNotExist():
            raise Http404()

    def get_form_kwargs(self):
        key = self.get_key()
        kwargs = super().get_form_kwargs()
        kwargs["initial"] = dict(first_name=key.first_name, last_name=key.last_name)
        return kwargs

    def form_valid(self, form):
        key = self.get_key()
        response = super().form_valid(form)
        for namespace in key.namespaces.all():
            self.object.namespaces.add(namespace)
        key.delete()
        return response

    def get_success_url(self):
        return reverse("login")


class MessageView(FormView):
    form_class = MessageForm
    template_name = "reservamelon/simple_form.html"

    def dispatch(self, request, *args, **kwargs):
        get_current_site(self.request).ext.refresh_from_db()
        if not get_current_site(self.request).ext.settings.allow_messages:
            raise PermissionDenied
        return super().dispatch(request, *args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["user"] = self.request.user
        return kwargs

    def form_valid(self, form):
        message = render_to_string(
            "reservamelon/message.txt",
            context=dict(user=self.request.user, **form.cleaned_data),
        )
        try:
            send_mail(
                f"Missatge de {get_current_site(self.request).name}",
                message,
                settings.EMAIL_FROM,
                [settings.EMAIL_TO],
                fail_silently=False,
            )
            messages.add_message(
                self.request,
                messages.SUCCESS,
                f"El teu missatge s'ha enviat correctament. Si és necessari contactarem amb tu a partir de la teva adreça d'e-mail: {self.request.user.email}. Gràcies!",
            )
        except Exception as e:
            messages.add_message(
                self.request,
                messages.ERROR,
                f"Hi ha hagut algun problema a l'enviar el teu missatge. Posat en contacte amb nosaltres per altres mitjans.",
            )
        return HttpResponseRedirect(redirect_to=reverse("home"))


class ChangeUserDataView(UpdateView):
    form_class = ChangeUserDataForm
    template_name = "reservamelon/profile.html"

    def get_object(self, queryset=None):
        return self.request.user

    def form_valid(self, form):
        response = super().form_valid(form)
        messages.add_message(
            self.request,
            messages.SUCCESS,
            f"Les teves dades s'han actualitzat",
        )
        return response

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["buttontext"] = "Actualitzar dades"
        context["password_form"] = ChangePasswordForm(user=self.request.user)
        return context

    def get_success_url(self):
        return reverse("profile")


class ChangePasswordView(ChangeUserDataView):
    form_class = ChangePasswordForm

    def get_form_kwargs(self):
        return {"data": self.request.POST, "user": self.request.user}

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # Here we swap forms, so it is able to deal with the password form as the view form
        password_form = context["form"]
        context["form"] = ChangeUserDataForm(
            initial=dict(
                first_name=self.request.user.first_name,
                last_name=self.request.user.last_name,
            )
        )
        context["password_form"] = password_form
        return context

    def form_valid(self, form):
        response = super(ChangeUserDataView, self).form_valid(form)
        messages.add_message(
            self.request,
            messages.SUCCESS,
            f"El teu password s'ha actualitzat",
        )
        login(self.request, self.request.user)
        return response

    def form_invalid(self, form):
        return super().form_invalid(form)


class MyReservationsView(TemplateView):
    template_name = "reservamelon/my_reservations.html"


class EditReservationView(UpdateView):
    model = Reservation
    template_name = "reservamelon/update_reservation.html"
    context_object_name = "reservation"
    form_class = ReservationUpdateForm

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs["user"] = self.request.user
        return kwargs

    def get_object(self, queryset=None):
        reservation = ReservationBase.objects.get(pk=decode_hashid(self.kwargs["hid"]))
        if reservation.name != self.request.user or reservation.has_passed:
            messages.add_message(
                self.request, messages.ERROR, "No pots editar aquesta reserva"
            )
            raise PermissionDenied
        return reservation

    def get_success_url(self):
        return f"{reverse('home')}?element={self.object.element.hashid}&moment={self.object.moment.date()}"
