from django.conf import settings
from django.contrib.sites.shortcuts import get_current_site


def context(request):
    not_needed = [request.path.endswith(ext) for ext in ["png", "jpg", "css", "js"]]
    if any(not_needed):
        return {}
    get_current_site(request).ext.refresh_from_db()
    site_settings = get_current_site(request).ext.settings
    return {
        "settings": {
            "logo_url": settings.LOGO_URL,
            "logo_shadow": settings.LOGO_SHADOW,
            "allow_messages": site_settings.allow_messages,
        }
    }
