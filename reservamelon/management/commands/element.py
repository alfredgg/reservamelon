from django.core.management.base import BaseCommand
from reservamelon.models import Element


class Command(BaseCommand):
    help = "Example command!"
    requires_migrations_checks = False

    def add_arguments(self, parser):
        parser.add_argument("NAME")

    def handle(self, *args, **options):
        name = options.get("NAME", "Rervamelon")
        Element.objects.create(name=name)
        self.stdout.write(f"Created: {name}")
