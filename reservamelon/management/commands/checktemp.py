from django.core.management.base import BaseCommand
from reservamelon.models import Element


class Command(BaseCommand):
    help = "Example command!"

    def handle(self, *args, **options):
        from django.template.loader import select_template

        template = select_template(["django/forms/widgets/text.html"])
        print(template.origin)
