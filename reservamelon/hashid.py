from django.conf import settings
from hashids import Hashids


hasher = Hashids(salt=settings.HASHID_SALT, min_length=10)


def encode_hashid(int_id):
    return hasher.encode(
        int_id,
    )


def decode_hashid(hash_id):
    value = hasher.decode(hash_id)
    if len(value) == 1:
        return value[0]
    return None
