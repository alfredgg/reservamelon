from django import forms
from reservamelon.models import Reservation, Element, ReservationBase, User
from datetime import datetime, timedelta
from django.core.exceptions import ValidationError
from django.utils import timezone
from django.db.models import Q
from reservamelon.hashid import decode_hashid
from django.forms.widgets import TextInput, PasswordInput
from django.contrib.auth.forms import (
    BaseUserCreationForm,
    UsernameField,
    PasswordChangeForm,
    AuthenticationForm,
)


class ReservamelonInputWidget(TextInput):
    template_name = "django/forms/widgets/front_input.html"


class ReservamelonUsernameField(UsernameField):
    widget = ReservamelonInputWidget


class ReservamelonPasswordWidget(PasswordInput):
    template_name = "django/forms/widgets/front_input.html"


hours_field_choices = [
    (
        "0.5",
        "30 minuts",
    ),
    (
        "1",
        "1 hora",
    ),
    ("1.5", "Hora i mitja"),
    (
        "2",
        "2 hores",
    ),
    (
        "2.5",
        "2 hores i mitja",
    ),
    (
        "3",
        "3 hores",
    ),
    (
        "3.5",
        "3 hores i mitja",
    ),
    (
        "4",
        "4 hores",
    ),
    (
        "4.5",
        "4 hores i mitja",
    ),
    (
        "5",
        "5 hores",
    ),
    (
        "5.5",
        "5 hores i mitja",
    ),
    (
        "6",
        "6 hores",
    ),
    (
        "6.5",
        "6 hores i mitja",
    ),
    (
        "7",
        "7 hores",
    ),
    (
        "7.5",
        "7 hores i mitja",
    ),
    (
        "8",
        "8 hores",
    ),
    (
        "8.5",
        "8 hores i mitja",
    ),
    (
        "9",
        "9 hores",
    ),
    (
        "9.5",
        "9 hores i mitja",
    ),
    (
        "10",
        "10 hores",
    ),
    (
        "10.5",
        "10 hores i mitja",
    ),
    (
        "11",
        "11 hores",
    ),
    (
        "11.5",
        "11 hores i mitja",
    ),
    (
        "12",
        "12 hores",
    ),
    (
        "12.5",
        "12 hores i mitja",
    ),
    (
        "13",
        "13 hores",
    ),
    (
        "13.5",
        "13 hores i mitja",
    ),
    (
        "14",
        "14 hores",
    ),
    (
        "14.5",
        "14 hores i mitja",
    ),
    (
        "15",
        "15 hores",
    ),
    (
        "15.5",
        "15 hores i mitja",
    ),
    (
        "16",
        "16 hores",
    ),
    (
        "16.5",
        "16 hores i mitja",
    ),
    (
        "17",
        "17 hores",
    ),
    (
        "17.5",
        "17 hores i mitja",
    ),
    (
        "18",
        "18 hores",
    ),
    (
        "18.5",
        "18 hores i mitja",
    ),
    (
        "19",
        "19 hores",
    ),
    (
        "19.5",
        "19 hores i mitja",
    ),
    (
        "20",
        "20 hores",
    ),
    (
        "20.5",
        "20 hores i mitja",
    ),
    (
        "21",
        "21 hores",
    ),
    (
        "21.5",
        "21 hores i mitja",
    ),
    (
        "22",
        "22 hores",
    ),
    (
        "22.5",
        "22 hores i mitja",
    ),
    (
        "23",
        "23 hores",
    ),
    (
        "23.5",
        "23 hores i mitja",
    ),
    (
        "24",
        "1 dia",
    ),
]


class ReservationForm(forms.ModelForm):
    class Meta:
        model = Reservation
        # exclude = ("created_time",)
        fields = (
            "element",
            "moment",
            "time",
            "hours",
            "notes",
        )
        widgets = {
            "moment": forms.DateInput(
                # format=("%d/%m/%Y"),
                attrs={
                    "class": "form-control shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline border-gray-400",
                    "placeholder": "Select a date",
                    "type": "date",
                },
            ),
        }

    time = forms.TimeField(
        widget=forms.TimeInput(
            {
                "class": "shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline border-gray-400",
                "type": "time",
            },
        ),
        label="A quina hora?",
        required=True,
    )
    hours = forms.DecimalField(
        widget=forms.Select(choices=hours_field_choices), label="Quant de temps?"
    )  # TODO: Create a concrete widget for this
    template_name_div = "reservamelon/reservation_form_as_div.html"
    element = forms.ChoiceField()

    def __init__(self, user, *args, **kwargs):
        super().__init__(*args, **kwargs)
        allowed_elements = Element.objects.filter(
            Q(namespace__isnull=True) | Q(namespace__in=user.namespaces.all())
        )
        self.fields["element"].choices = [("", "----")] + [
            (element.hashid, element.name) for element in allowed_elements
        ]
        if len(list(self.fields["element"].choices)) == 2:
            self.fields["element"].choices = list(self.fields["element"].choices)[1:]
            self.fields["element"].initial = self.fields["element"].choices[0]

        now = timezone.localtime(timezone.now())
        time_to_set = (timezone.localtime(timezone.now()) + timedelta(hours=1)).replace(
            second=0, minute=0
        )
        at_half = timezone.localtime(timezone.now().replace(second=0, minute=31))
        if now < at_half:
            time_to_set = timezone.localtime(
                timezone.now().replace(second=0, minute=30)
            )

        self.fields["time"].initial = time_to_set.time()
        self.fields["moment"].initial = time_to_set.date()

    def clean_element(self):
        return Element.objects.get(pk=decode_hashid(self.data["element"]))

    def clean(self):
        super().clean()

        if self.cleaned_data["hours"] > 24:
            raise ValidationError("El temps escollit és erroni")

        one_hour_ago = timezone.localtime(timezone.now()) - timedelta(hours=1)
        begin_moment = datetime.combine(
            self.cleaned_data["moment"],
            self.cleaned_data["time"],
            tzinfo=timezone.get_current_timezone(),
        )
        if begin_moment < one_hour_ago:
            raise ValidationError("No pots reservar en el passat")
        end_moment = begin_moment + timedelta(
            minutes=int(float(self.cleaned_data["hours"]) * 60)
        )
        self.cleaned_data["moment"] = begin_moment
        intersections = ReservationBase.get_intersections(
            element=self.cleaned_data["element"],
            moment_start=begin_moment,
            moment_end=end_moment,
        )
        if intersections.exists():
            raise ValidationError("El rang horari escollit ja està ocupat.")


class ReservationUpdateForm(forms.ModelForm):
    template_name_div = "reservamelon/form_as_div.html"

    class Meta:
        model = Reservation
        fields = (
            "time",
            "hours",
            "notes"
        )

    time = forms.TimeField(
        widget=forms.TimeInput(
            {
                "class": "shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline border-gray-400",
                "type": "time",
            },
        ),
        label="A quina hora?",
        required=True,
    )
    hours = forms.DecimalField(
        widget=forms.Select(choices=hours_field_choices), label="Quant de temps?"
    )

    def __init__(self, user, *args, **kwargs):
        super().__init__(*args, **kwargs)
        instance = kwargs["instance"]
        moment = timezone.localtime(instance.moment)
        self.fields["time"].initial = moment.time()
        self.fields["hours"].initial = self.instance.hours
        self.user = user
        self.moment = None
        self.moment_end = None

    def clean(self):
        super().clean()

        if self.user != self.instance.name:
            raise ValidationError("Usuari erroni")

        if self.cleaned_data["hours"] > 24:
            raise ValidationError("El temps escollit és erroni")

        one_hour_ago = timezone.localtime(timezone.now()) - timedelta(hours=1)
        begin_moment = datetime.combine(
            self.instance.moment.date(),
            self.cleaned_data["time"],
            tzinfo=timezone.get_current_timezone(),
        )
        if begin_moment < one_hour_ago:
            raise ValidationError("No pots reservar en el passat")
        end_moment = begin_moment + timedelta(
            minutes=int(float(self.cleaned_data["hours"]) * 60)
        )
        self.cleaned_data["moment"] = begin_moment
        intersections = ReservationBase.get_intersections(
            element=self.instance.element,
            moment_start=begin_moment,
            moment_end=end_moment,
            avoid_reservations=[self.instance] + list(self.instance.children.all())
        )
        if intersections.exists():
            raise ValidationError("El rang horari escollit ja està ocupat.")

        self.moment = begin_moment
        self.moment_end = end_moment

    def save(self, commit=True):
        self.instance.moment = self.moment
        self.instance.moment_end = self.moment_end
        return super().save(commit)


class MessageForm(forms.Form):
    element = forms.ChoiceField(
        label="Sobre què vols fer el comentari?", widget=forms.Select(), required=False
    )
    subject = forms.CharField(label="Títol", widget=ReservamelonInputWidget())
    message = forms.CharField(widget=forms.Textarea())

    template_name_div = "reservamelon/form_as_div.html"

    def __init__(self, user, *args, **kwargs):
        super().__init__(*args, **kwargs)
        allowed_elements = Element.objects.filter(
            Q(namespace__isnull=True) | Q(namespace__in=user.namespaces.all())
        )
        self.fields["element"].choices = [("", "Comentari general")] + [
            (element.hashid, element.name) for element in allowed_elements
        ]
        if len(list(self.fields["element"].choices)) == 2:
            self.fields["element"].choices = list(self.fields["element"].choices)[1:]
            self.fields["element"].initial = self.fields["element"].choices[0]

    def clean_element(self):
        if not self.data["element"]:
            return None
        return Element.objects.get(pk=decode_hashid(self.data["element"]))


class SignUpForm(BaseUserCreationForm):
    class Meta:
        model = User
        fields = ("first_name", "last_name", "email", "username")
        widgets = {
            "first_name": ReservamelonInputWidget(),
            "last_name": ReservamelonInputWidget(),
            "email": ReservamelonInputWidget(),
        }
        field_classes = {"username": ReservamelonUsernameField}

    template_name_div = "reservamelon/form_as_div.html"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["password1"].widget = ReservamelonPasswordWidget()
        self.fields["password2"].widget = ReservamelonPasswordWidget()


class ChangeUserDataForm(forms.ModelForm):
    class Meta:
        model = User
        fields = (
            "first_name",
            "last_name",
            "email"
        )
        widgets = {
            "first_name": ReservamelonInputWidget(),
            "last_name": ReservamelonInputWidget(),
            "email": ReservamelonInputWidget(),
        }
        field_classes = {"username": ReservamelonUsernameField}

    template_name_div = "reservamelon/form_as_div.html"


class ChangePasswordForm(PasswordChangeForm):
    template_name_div = "reservamelon/form_as_div.html"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["old_password"].widget = ReservamelonPasswordWidget()
        self.fields["new_password1"].widget = ReservamelonPasswordWidget()
        self.fields["new_password2"].widget = ReservamelonPasswordWidget()


class LoginForm(AuthenticationForm):
    template_name_div = "reservamelon/form_as_div.html"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["username"] = ReservamelonUsernameField()
        self.fields["password"].widget = ReservamelonPasswordWidget()


class PasswordlessForm(forms.Form):
    template_name_div = "reservamelon/form_as_div.html"

    user = forms.ModelChoiceField(
        queryset=User.objects.filter(is_superuser=False).all(), empty_label="---"
    )

    def __init__(self, request, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def get_user(self):
        return self.cleaned_data["user"]
