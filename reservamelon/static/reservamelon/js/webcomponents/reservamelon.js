import { createCustomElement } from 'https://cdn.skypack.dev/ficusjs@3/custom-element'
import { html, renderer } from 'https://cdn.skypack.dev/@ficusjs/renderers@3/htm'

createCustomElement('reservamelon-component', {
  renderer,
  render () {
    return html`
    <div>Hello from Rervamelon!</div>
  `
  },
  mounted() {
  }
})