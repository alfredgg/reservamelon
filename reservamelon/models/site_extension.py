from django.db import models
from django.contrib.sites.models import Site
from django_pydantic_field import SchemaField
from reservamelon.schemas import SettingsSchema


class SiteExtension(models.Model):
    site = models.OneToOneField(
        Site, primary_key=True, related_name="ext", on_delete=models.CASCADE
    )
    settings = SchemaField(schema=SettingsSchema)
