from django.db import models
from decimal import Decimal, ROUND_HALF_UP
from django.db.models import Q
from django.contrib.auth.models import AbstractUser
from django.contrib.sites.models import Site
from markdown import markdown
from django.utils.translation import gettext_lazy as _
import re
from django.utils import timezone
from . import pre
from django.core.exceptions import ValidationError
from datetime import datetime, time, timedelta
from django.db.models.signals import post_save
from django.conf import settings


class Namespace(pre.HashidMixin, models.Model):
    class Meta:
        ordering = ("name",)
        verbose_name = "Espai de noms"
        verbose_name_plural = "Espais de noms"

    name = models.CharField(max_length=50, null=False, blank=False)
    site = models.ForeignKey(
        Site,
        null=False,
        blank=False,
        on_delete=models.CASCADE,
        related_name="namespaces",
        default=settings.SITE_ID
    )

    def __str__(self):
        return self.name


class User(pre.HashidMixin, AbstractUser):
    class Meta:
        ordering = ("first_name",)
        verbose_name = "Usuari"
        verbose_name_plural = "Usuaris"

    namespaces = models.ManyToManyField(
        Namespace, related_name="users", blank=True, verbose_name="Espais de noms"
    )
    first_name = models.CharField(
        _("first name"), max_length=150, blank=False, null=False
    )
    last_name = models.CharField(
        _("last name"), max_length=150, blank=True, null=True
    )
    email = models.EmailField(_("email address"), blank=False, null=False, unique=True)

    @property
    def name(self):
        return f"{self.first_name} {self.last_name or ''}"

    def __str__(self):
        return self.name

    @property
    def future_reservations(self):
        now = timezone.localtime(timezone.now()).date()
        return self.reservations.filter(moment__gte=now).order_by("moment").all()

    @property
    def past_reservations(self):
        now = timezone.localtime(timezone.now()).date()
        return self.reservations.filter(moment__lt=now).order_by("-moment").all()


class Picture(pre.HashidMixin, models.Model):
    class Meta:
        verbose_name = "Imatge"
        verbose_name_plural = "Imatges"

    name = models.CharField(max_length=50, null=False, blank=False)
    image = models.ImageField()
    uploaded_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name


class Element(pre.HashidMixin, models.Model):
    class Meta:
        verbose_name = "Element"
        verbose_name_plural = "Elements"

    objects = pre.ElementManager()
    all_objects = pre.ElementAdminManager()

    name = models.CharField(max_length=250, verbose_name="Element")
    description = models.TextField(blank=True, null=True)
    threshold = models.IntegerField(
        default=5,
        verbose_name="Llindar de reserves",
        help_text="Color per un dia depenent de si el número de reserves està per sota o per sobre d'aquest llindar.",
    )
    picture = models.ForeignKey(
        Picture, on_delete=models.DO_NOTHING, default=None, null=True, blank=True
    )
    created_time = models.DateTimeField(
        auto_now_add=True, verbose_name="Data de creació"
    )
    namespace = models.ForeignKey(
        Namespace,
        on_delete=models.CASCADE,
        default=None,
        null=True,
        blank=True,
        related_name="elements",
        verbose_name="Espai de noms",
    )
    active = models.BooleanField(default=True, verbose_name="Actiu")
    pre_margin_minutes = models.IntegerField(
        default=0,
        verbose_name="Minuts de marge abans de la reserva",
        blank=True,
        null=True,
    )
    post_margin_minutes = models.IntegerField(
        default=0,
        verbose_name="Minuts de marge després de la reserva",
        blank=True,
        null=True,
    )

    def is_user_allowed(self, user):
        return not self.namespace or self.namespace.users.filter(pk=user.pk).exists()

    def __str__(self):
        return self.name

    @property
    def html_description(self):
        s = self.description
        patterns = [
            (r"(?<!br)>", "&gt;"),  # > not preceded by br
            # r"(?<!\/)>",  # > not preceded by /
            (r"<(?!br\b)", "&lt;"),  # < followed by br
        ]
        for pattern, replacement in patterns:
            s = re.sub(pattern, replacement, s)
        return markdown(s)


class ReservationBase(pre.HashidMixin, models.Model):
    class Meta:
        ordering = ("moment",)
        db_table = "reservamelon_reservation"

    element = models.ForeignKey(
        Element,
        on_delete=models.SET_NULL,
        related_name="reservations",
        null=True,
        verbose_name="Què reserves?",
    )
    name = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name="reservations",
        null=True,
        verbose_name="Qui reserva?",
    )
    moment = models.DateTimeField(verbose_name="Quin dia?")
    emoji = models.CharField(
        max_length=5,
        blank=True,
        null=True,
        choices=pre.EmojiChoices.choices,
        verbose_name="Emoji",
    )
    title = models.CharField(max_length=100, verbose_name="Raó", null=True, blank=True)
    moment_end = models.DateTimeField(verbose_name="Moment final", null=False)
    notes = models.TextField(
        blank=True, null=True, verbose_name="Notes a afegir a la teva reserva:"
    )
    created_time = models.DateTimeField(
        auto_now_add=True, verbose_name="Data de creació"
    )
    updated_time = models.DateTimeField(
        auto_now=True, verbose_name="Data d'actualització"
    )
    parent = models.ForeignKey(
        "ReservationBase",
        null=True,
        default=None,
        on_delete=models.CASCADE,
        related_name="children",
    )

    def intersects(self, moment_start, moment_end):
        starts_before = moment_start <= self.moment < moment_end
        is_in_middle = self.moment <= moment_start < moment_end <= self.moment_end
        self_is_in_middle = moment_start <= self.moment < self.moment_end <= moment_end
        ends_after = moment_start < self.moment_end <= moment_end
        return starts_before or is_in_middle or self_is_in_middle or ends_after

    @classmethod
    def get_intersections(
        cls, element, moment_start, moment_end, avoid_reservations=None
    ):
        queryset = ReservationBase.objects.filter(element=element).filter(
            Q(moment__lte=moment_end, moment_end__gte=moment_start)
            | Q(moment__gte=moment_start, moment_end__lte=moment_end)
            | Q(moment__lte=moment_start, moment_end__gte=moment_end)
        )
        if avoid_reservations:
            pks = [reservation.pk for reservation in avoid_reservations]
            queryset = queryset.exclude(pk__in=pks)
        return queryset

    @property
    def localmoments(self):
        return timezone.localtime(self.moment), timezone.localtime(self.moment_end)

    @property
    def is_reservation(self):
        return self.name is not None

    @property
    def is_block(self):
        return self.name is None and not self.parent

    @property
    def is_subreservation(self):
        return self.name is None and self.parent is not None

    @property
    def hours(self):
        difference = self.moment_end - self.moment
        hours = Decimal(difference.seconds) / 60 / 60 + (difference.days * 24)
        return hours

    def hours_in_day(self, day):
        day_start = self.moment.astimezone(timezone.get_current_timezone()).date()
        day_end = self.moment_end.astimezone(timezone.get_current_timezone()).date()
        if day_start > day or day_end < day:
            return 0
        if day_start == day == day_end:
            return self.hours
        if day_start == day and day_end != day:
            last_hour = datetime.combine(day, time.max).astimezone(timezone.utc)
            return Decimal((last_hour - self.moment).seconds / 60 / 60).quantize(
                Decimal("0.1"), rounding=ROUND_HALF_UP
            )
        elif day_end == day and day_start != day:
            first_hour = datetime.combine(day, time.min).astimezone(timezone.utc)
            return Decimal((self.moment_end - first_hour).seconds / 60 / 60).quantize(
                Decimal("0.1"), rounding=ROUND_HALF_UP
            )
        else:
            return 24

    def hours_per_day(self):
        moment_end = self.moment_end.astimezone(timezone.get_current_timezone())
        moment_start = self.moment.astimezone(timezone.get_current_timezone())
        if moment_end == moment_start:
            return [
                (
                    moment_start,
                    self.hours,
                )
            ]
        current_day = moment_start.date()
        hours = self.hours_in_day(current_day)
        accum = [
            (
                current_day,
                hours,
            )
        ]
        while hours:
            current_day += timedelta(days=1)
            hours = self.hours_in_day(current_day)
            accum.append((current_day, hours))
        return accum

    def clean(self):
        if self.moment_end is None:
            return
        if self.moment > self.moment_end:
            raise ValidationError("Les dates son incorrectes")
        avoid_reservations = [self]
        if self.pk:
            avoid_reservations += list(self.children.all())
        if ReservationBase.get_intersections(
            self.element,
            self.moment,
            self.moment_end,
            avoid_reservations=avoid_reservations,
        ).exists():
            raise ValidationError("Aquesta franja ja està ocupada")

    @property
    def has_passed(self):
        now = timezone.localtime(timezone.now()).date()
        return self.moment.date() < now

    @classmethod
    def save_margins(cls, sender, instance, **kwargs):
        if not instance.is_reservation:
            return

        ReservationBase.objects.filter(parent=instance.pk).delete()
        if instance.element.pre_margin_minutes:
            SubReservation.objects.create(
                element=instance.element,
                moment=instance.moment
                       - timedelta(minutes=instance.element.pre_margin_minutes),
                moment_end=instance.moment,
                title="Temps de marge abans d'una reserva",
                parent=instance,
            )
        if instance.element.post_margin_minutes:
            SubReservation.objects.create(
                element=instance.element,
                moment=instance.moment_end,
                moment_end=instance.moment_end
                           + timedelta(minutes=instance.element.post_margin_minutes),
                title="Temps de marge després d'una reserva",
                parent=instance,
            )


class Reservation(ReservationBase):
    class Meta:
        proxy = True
        verbose_name = "Reserva"
        verbose_name_plural = "Reserves"
        ordering = ("moment",)

    objects = pre.ReservationManager()

    def intersects_block(self):
        return False

    def __str__(self):
        moment_end = (
            self.moment_end.time()
            if self.moment.date() == self.moment_end.date()
            else f"({self.moment_end})"
        )
        return f"{self.element.name}: {self.name}, {self.moment.date()} [{self.moment.time()} - {moment_end}]"


class SubReservation(ReservationBase):
    class Meta:
        proxy = True

    objects = pre.SubReservationManager()


class Block(ReservationBase):
    class Meta:
        proxy = True
        verbose_name = "Bloqueig"
        verbose_name_plural = "Bloquejos"
        ordering = ("moment",)

    objects = pre.BlockManager()

    def __str__(self):
        return f"{self.element.name}: {self.moment.date()} [{self.moment.time()} - {self.moment_end}]"


class Key(models.Model):
    first_name = models.CharField(max_length=50, blank=True, null=True)
    last_name = models.CharField(max_length=50, blank=True, null=True)
    token = models.CharField(max_length=20, db_index=True, unique=True)
    created_time = models.DateTimeField(
        auto_now_add=True, verbose_name="Data de creació"
    )
    namespaces = models.ManyToManyField(
        Namespace,
        blank=True,
        null=True,
        verbose_name="Espais de noms",
        related_name="+"
    )

    def __str__(self):
        return f"{self.first_name} {self.last_name}"


post_save.connect(ReservationBase.save_margins, sender=ReservationBase)
post_save.connect(ReservationBase.save_margins, sender=Reservation)
post_save.connect(ReservationBase.save_margins, sender=Block)
post_save.connect(ReservationBase.save_margins, sender=SubReservation)
