from django.db import models
from reservamelon.hashid import encode_hashid


class ElementManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(active=True)


class ElementAdminManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset()


class ReservationManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(name__isnull=False)


class SubReservationManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(name__isnull=True, parent__isnull=False)


class BlockManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(name__isnull=True, parent__isnull=True)


class EmojiChoices(models.TextChoices):
    lock = "🔒️", "🔒️"
    wrench = "🔧", "🔧"
    thermometer = "🌡️", "🌡️"
    food = "🥗", "🥗"
    cat = "🐱", "🐱"
    party = "🎉", "🎉"
    alert = "⚠️", "⚠️"
    cross = "❌", "❌"


class HashidMixin:
    @property
    def hashid(self):
        return encode_hashid(self.pk)
