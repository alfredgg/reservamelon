from django.apps import AppConfig


class ReservemelonAppConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "reservamelon"
