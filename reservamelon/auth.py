from django.contrib.auth.models import Permission


ACTIONS = (
    "add",
    "change",
    "delete",
    "view",
)
STAFF_OBJECTS = ("logentry", "reservation", "user", "element", "key", "block")
STAFF_FORBIDDEN = [
    "delete_element",
    "delete_user",
    "add_user",
    "add_key",
    "change_reservation",
]
STAFF_PERMISSIONS = [
    f"{action}_{obj}"
    for obj in STAFF_OBJECTS
    for action in ACTIONS
    if f"{action}_{obj}" not in STAFF_FORBIDDEN
]


def set_permissions_to_user(user):
    user.user_permissions.clear()
    if user.is_staff:
        permissions = Permission.objects.filter(codename__in=STAFF_PERMISSIONS).all()
        user.user_permissions.add(*permissions)
        user.save()
