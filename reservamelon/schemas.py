from pydantic import BaseModel, RootModel, Field, field_validator
from decimal import Decimal
import typing as t
from datetime import date as pdate, time, datetime
from dateutil.relativedelta import relativedelta
import calendar


class SettingsSchema(BaseModel):
    passwordless: bool = Field(default=False)
    token_expiration_days: int = Field(default=10)
    allow_messages: bool = Field(default=False)


class ExportInstanceSchema(BaseModel):
    class Config:
        from_attributes = True

    element: t.Union[t.Any, str]
    person: t.Union[t.Any, str] = Field(alias="name")
    day: t.Union[pdate, datetime] = Field(alias="moment")
    hour: t.Union[time, datetime] = Field(alias="moment")
    hours: Decimal

    """
    TODO: I dislike how this validation is used. Fields typing is set as a broad type for allowing
    this kind of parsing. 
    Another way would be to create a model_validator(mode=before), then do the parsing there. 
    However, it would make it more verbose.
    """

    @field_validator("day")
    @classmethod
    def validate_day(cls, v):
        return v.date()

    @field_validator("hour")
    @classmethod
    def validate_hour(cls, v):
        return v.time()

    @field_validator("element")
    @classmethod
    def validate_element(cls, v):
        return v.name

    @field_validator("person")
    @classmethod
    def validate_person(cls, v):
        return v.name


class ExportSchema(RootModel):
    root: t.List[ExportInstanceSchema]

    def __iter__(self):
        return iter(self.root)

    def __getitem__(self, item):
        return self.root[item]


class DaySchema(BaseModel):
    number: int
    is_current_month: bool = Field(default=True)
    color: str = Field(default="")
    date: t.Optional[pdate] = Field(default=None)

    @property
    def sdate(self):
        return str(self.date)

    @property
    def has_passed(self):
        return self.date < datetime.now().date()


class CalendarSchema(BaseModel):
    month: int
    year: int
    day_names: t.List[str] = ["DL", "DT", "DC", "DJ", "DV", "DS", "DG"]
    rows: t.List[t.List[DaySchema]]

    @property
    def month_name(self):
        month_names = [
            "",
            "Gener",
            "Febrer",
            "Març",
            "Abril",
            "Maig",
            "Juny",
            "Juliol",
            "Agost",
            "Setembre",
            "Octubre",
            "Novembre",
            "Desembre",
        ]
        return month_names[self.month]

    @property
    def next_month(self):
        current_month = pdate(self.year, self.month, 1)
        return current_month + relativedelta(months=1)

    @property
    def previous_month(self):
        current_month = pdate(self.year, self.month, 1)
        return current_month - relativedelta(months=1)

    @classmethod
    def create_for(cls, month, year):
        cal = calendar.monthcalendar(year, month)
        built_calendar = CalendarSchema(
            month=month,
            year=year,
            rows=[
                [
                    DaySchema(number=day, date=pdate(year, month, day) if day else None)
                    for day in row
                ]
                for row in cal
            ],
        )

        if 0 in cal[0]:
            previous_month = built_calendar.previous_month
            prev_cal = calendar.monthcalendar(previous_month.year, previous_month.month)
            for idx, day in enumerate(built_calendar.rows[0]):
                if day.number != 0:
                    break
                day.number = prev_cal[-1][idx]
                day.is_current_month = False
                day.date = pdate(
                    previous_month.year, previous_month.month, prev_cal[-1][idx]
                )

        if 0 in cal[-1]:
            next_month = built_calendar.next_month
            next_calendar = calendar.monthcalendar(next_month.year, next_month.month)
            for idx, day in enumerate(built_calendar.rows[-1]):
                if day.number != 0:
                    continue
                day.number = next_calendar[0][idx]
                day.is_current_month = False
                day.date = pdate(
                    next_month.year, next_month.month, next_calendar[0][idx]
                )

        return built_calendar
