# Generated by Django 4.2.7 on 2024-03-31 08:18

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [
        ("reservamelon", "0018_subreservation_element_post_margin_minutes_and_more"),
    ]

    operations = [
        migrations.AlterField(
            model_name="reservationbase",
            name="name",
            field=models.ForeignKey(
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                related_name="reservations",
                to=settings.AUTH_USER_MODEL,
                verbose_name="Qui reserva?",
            ),
        ),
        migrations.AlterField(
            model_name="user",
            name="last_name",
            field=models.CharField(
                blank=True, max_length=150, null=True, verbose_name="last name"
            ),
        ),
    ]
