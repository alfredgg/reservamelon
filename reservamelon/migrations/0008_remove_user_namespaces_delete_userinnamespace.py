# Generated by Django 4.2.7 on 2024-03-25 13:55

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("reservamelon", "0007_alter_element_options_alter_picture_options_and_more"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="user",
            name="namespaces",
        ),
        migrations.DeleteModel(
            name="UserInNamespace",
        ),
    ]
