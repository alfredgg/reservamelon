# Generated by Django 4.2.7 on 2024-03-25 14:10

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [
        ("reservamelon", "0008_remove_user_namespaces_delete_userinnamespace"),
    ]

    operations = [
        migrations.AlterModelOptions(
            name="namespace",
            options={
                "ordering": ("name",),
                "verbose_name": "Espai de noms",
                "verbose_name_plural": "Espais de noms",
            },
        ),
        migrations.AddField(
            model_name="element",
            name="namespace",
            field=models.ForeignKey(
                blank=True,
                default=None,
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                related_name="elements",
                to="reservamelon.namespace",
                verbose_name="Espai de noms",
            ),
        ),
        migrations.AddField(
            model_name="user",
            name="namespaces",
            field=models.ManyToManyField(
                blank=True,
                related_name="users",
                to="reservamelon.namespace",
                verbose_name="Espais de noms",
            ),
        ),
    ]
