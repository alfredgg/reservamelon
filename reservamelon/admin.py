from django.contrib import admin
from reservamelon import models
from django.contrib.auth.admin import UserAdmin as DjangoUserAdmin
from django.contrib.sites.admin import (
    SiteAdmin,
)  # It must be imported so it can be unregistered
from django.template.loader import render_to_string
from django.contrib.sites.models import Site
from django.utils.safestring import SafeString
from django.urls import reverse
from .auth import set_permissions_to_user
from django.utils.translation import gettext as _
from django.urls import path
from reservamelon.views import admin_views as views
from django.core.exceptions import PermissionDenied
from datetime import timedelta
from django.contrib.sites.shortcuts import get_current_site
from django.utils import timezone


admin.site.site_header = "Reservamelón"
admin.site.index_title = "Administració"
admin.site.unregister(Site)


@admin.register(models.User)
class UserAdmin(DjangoUserAdmin):
    change_list_template = "admin/users_change_list.html"
    list_filter = ("is_staff", "is_active", "namespaces")
    list_display = (
        "username",
        "email",
        "first_name",
        "last_name",
        "is_staff",
        "is_active",
        "user_namespaces",
    )

    def user_namespaces(self, object):
        return ", ".join(namespace.name for namespace in object.namespaces.all())

    user_namespaces.short_description = "Espais de noms"

    def save_model(self, request, obj, form, change):
        if not request.user.is_superuser and obj.is_superuser:
            raise PermissionDenied
        return super().save_model(request, obj, form, change)

    def user_change_password(self, request, id, form_url=""):
        obj = models.User.objects.get(pk=id)
        if not request.user.is_superuser and obj.is_superuser:
            raise PermissionDenied
        return super().user_change_password(request, id, form_url)

    def save_related(self, request, form, formsets, change):
        super().save_related(request, form, formsets, change)
        set_permissions_to_user(form.instance)

    def get_fieldsets(self, request, obj=None):
        if not obj:
            return self.add_fieldsets
        if not request.user.is_superuser:
            return (
                (None, {"fields": ("username", "password")}),
                (_("Personal info"), {"fields": ("first_name", "last_name", "email")}),
                (
                    _("Permissions"),
                    {
                        "fields": (
                            "is_active",
                            "is_staff",
                        )
                    },
                ),
                (_("Namespaces"), {"fields": ("namespaces",)}),
                (_("Important dates"), {"fields": ("last_login", "date_joined")}),
            )
        return (
            (None, {"fields": ("username", "password")}),
            (_("Personal info"), {"fields": ("first_name", "last_name", "email")}),
            (
                _("Permissions"),
                {
                    "fields": (
                        "is_active",
                        "is_staff",
                        "is_superuser",
                        "groups",
                        "user_permissions",
                    ),
                },
            ),
            (_("Namespaces"), {"fields": ("namespaces",)}),
            (_("Important dates"), {"fields": ("last_login", "date_joined")}),
        )

    def get_urls(self):
        urls = super().get_urls()
        custom_urls = [
            path(
                "generate_user",
                self.admin_site.admin_view(views.GenerateUserView.as_view()),
                name="generate_user",
            ),
        ]
        return custom_urls + urls


@admin.register(Site)
class SiteAdmin(admin.ModelAdmin):
    list_display = ("domain", "name")
    search_fields = ("domain", "name")
    readonly_fields = ["settings"]

    def settings(self, object):
        return SafeString(
            f"<a href='{reverse('admin:settings', args=(object.pk,))}'>Settings</a>"
        )

    settings.short_description = "⚙️"

    def get_urls(self):
        urls = super().get_urls()
        custom_urls = [
            path(
                "<id>/settings",
                self.admin_site.admin_view(
                    views.is_superuser(views.SettingsView.as_view())
                ),
                name="settings",
            ),
        ]
        return custom_urls + urls


@admin.register(models.Element)
class ElementsAdmin(admin.ModelAdmin):
    fields = (
        "name",
        "threshold",
        "namespace",
        "picture",
        "description",
        "html_description",
        "active",
        "pre_margin_minutes",
        "post_margin_minutes",
    )
    readonly_fields = ("html_description",)
    list_display = ("name", "namespace", "active")
    list_filter = ("namespace", "active")

    def html_description(self, object):
        if not object or not object.pk:
            return ""
        return SafeString(object.html_description)

    html_description.short_description = "Description"

    def get_queryset(self, request):
        return models.Element.all_objects


@admin.register(models.Reservation)
class ReservationsAdmin(admin.ModelAdmin):
    change_list_template = "admin/reservations_change_list.html"
    list_display = (
        "moment",
        "moment_end",
        "element",
        "name",
    )
    list_filter = ("element",)
    fields = ("element", "name", "moment", "moment_end", "notes")
    ordering = ("moment",)
    readonly_fields = (
        "moment",
        "moment_end",
    )

    def get_ordering(self, request):
        return ("-created_time",)

    def get_urls(self):
        urls = super().get_urls()
        custom_urls = [
            path(
                "export",
                self.admin_site.admin_view(views.ExportView.as_view()),
                name="export",
            ),
        ]
        return custom_urls + urls


@admin.register(models.Block)
class BlockAdmin(admin.ModelAdmin):
    list_display = (
        "block",
        "element",
        "moment",
        "moment_end",
    )
    list_filter = ("element",)
    fields = ("element", "moment", "moment_end", "emoji", "title", "notes")
    ordering = ("moment",)

    def block(self, object):
        if not object or not object.pk:
            return ""
        emoji = "" if not object.emoji else f" {object.emoji} "
        return f"{emoji}{object.title}"

    block.short_description = "Bloqueig"

    def render_change_form(
        self, request, context, add=False, change=False, form_url="", obj=None
    ):
        context["adminform"].fields["moment"].label = "Moment inicial"
        context["adminform"].fields["element"].label = "Element"
        return super().render_change_form(request, context, add, change, form_url, obj)


@admin.register(models.Picture)
class PicturesAdmin(admin.ModelAdmin):
    list_display = ("name",)


@admin.register(models.Namespace)
class NamespaceAdmin(admin.ModelAdmin):
    list_display = ("name",)
    fields = ("name",)


@admin.register(models.Key)
class KeyAdmin(admin.ModelAdmin):
    list_display = (
        "created_time",
        "expired",
        "first_name",
        "last_name",
    )
    ordering = ("-created_time",)
    fields = ("first_name", "last_name", "namespaces", "get_token", "created_time")
    readonly_fields = ("created_time", "get_token")

    def get_list_display(self, request):
        self.request = request
        return super().get_list_display(request)

    def get_form(self, request, obj=None, change=False, **kwargs):
        self.request = request
        return super().get_form(request, obj, change, **kwargs)

    def expired(self, object):
        if not object or not object.pk:
            return ""
        site = get_current_site(self.request)
        site.ext.refresh_from_db()
        return (timezone.now() - object.created_time) > timedelta(
            days=site.ext.settings.token_expiration_days
        )

    expired.short_description = "Expired"

    def get_token(self, object):
        return render_to_string(
            "admin/user_key.html",
            context=dict(
                url=self.request.build_absolute_uri(
                    reverse("create_user", args=(object.token,))
                )
            ),
        )

    get_token.short_description = "Clau d'usuari"
