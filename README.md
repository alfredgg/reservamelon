# Rervamelon 🍈

Reservamelon is a software application designed for booking various items. These items can include 
places, rooms, tools, time slots, and anything else you may need to reserve.

## Shut up and help me get started!

1. Create the docker image: `docker build -t reservamelon .`
2. In one session start the container: `docker run -ti -p 8000:8000 --name reservamelon reservamelon`
3. In another session create the DB: `docker exec -ti reservamelon python manage.py migrate`
4. In that last session create an admin user: `docker exec -ti reservamelon python manage.py createsuperuser`
5. Open the UI accessing to: http://127.0.0.1:8000
6. Open the admin UI accessing to: http://127.0.0.1:8000/admin
7. On the admin, create an element.
8. Now you are able to reserve it.
