FROM python:3.9-bullseye

ENV PYTHONUNBUFFERED 1

RUN mkdir -p /srv/app

RUN pip install --upgrade pip
RUN pip install poetry

WORKDIR /srv

COPY pyproject.toml /srv/pyproject.toml
COPY poetry.lock /srv/poetry.lock

RUN poetry config virtualenvs.create false
RUN poetry install

COPY . . 
RUN python manage.py collectstatic --noinput

CMD poetry run gunicorn reservamelon.wsgi --bind 0.0.0.0:8000 --access-logfile -