rundb:
	docker-compose -f docker/docker-compose.yml up reservamelon_postgres
	
scss:
	sass reservamelon/static/reservamelon/styles/sass/main.scss:reservamelon/static/reservamelon/styles/stylesheet.css --watch

tailwind:
	npx tailwindcss -i reservamelon/static/reservamelon/css/input.css  -o reservamelon/static/reservamelon/css/styles.css --watch

build:
	docker build -t reservamelon .

install:
	docker run -d --name reservamelon reservamelon
	docker exec reservamelon python manage.py migrate
	docker exec -e DJANGO_SUPERUSER_PASSWORD=mysecretpassword reservamelon python manage.py createsuperuser --username root --email root@admin.com --no-input
	docker commit reservamelon reservamelon
	docker stop reservamelon

run:
	docker run --rm -ti -p 8000:8000 reservamelon || true

dev:
	docker run --rm -ti -p 8000:8000 -v ${PWD}:/srv reservamelon python manage.py runserver 0.0.0.0:8000

devnorun:
	docker run --rm -ti -p 8000:8000 -v ${PWD}:/srv reservamelon